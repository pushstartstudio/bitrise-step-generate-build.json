#!/bin/bash
set -ex

BUILD_JSON_PATH="${build_path}/${build_json_filename}"

# Get package.json data
if [ ! -z "$package_json_path" ]; then
    PACKAGE_NAME=$(node -p "require('${package_json_path}').name")
    PACKAGE_VERSION=$(node -p "require('${package_json_path}').version")
fi

# Generate JSON
JSON_TEMPLATE='{"name":"%s","version":"%s","build":"%s","commit":"%s","workflow":"%s"}'
JSON=$(printf "$JSON_TEMPLATE" "$PACKAGE_NAME" "$PACKAGE_VERSION" "$BITRISE_BUILD_NUMBER" "$BITRISE_GIT_COMMIT" "$BITRISE_TRIGGERED_WORKFLOW_ID")

# Save JSON
echo "$JSON" > $BUILD_JSON_PATH

envman add --key BUILD_JSON_PATH --value $BUILD_JSON_PATH